\select@language {french}
\contentsline {section}{\numberline {1}Analyse}{2}
\contentsline {section}{\numberline {2}Analyse fonctionnelle}{2}
\contentsline {subsection}{\numberline {2.1}Structures}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Memoire}{2}
\contentsline {subsubsection}{\numberline {2.1.2}Variable}{3}
\contentsline {subsection}{\numberline {2.2}Fonctions}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Fonction ``allouer''}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Fonction ``supprimer''}{4}
\contentsline {subsubsection}{\numberline {2.2.3}Fonction ``type\_a\_chaine''}{4}
\contentsline {subsubsection}{\numberline {2.2.4}Fonction ``lire\_valeur''}{4}
\contentsline {section}{\numberline {3}Dossier de programmation}{4}
\contentsline {subsection}{\numberline {3.1}Fonction ``allouer''}{4}
\contentsline {subsection}{\numberline {3.2}Fonction ``supprimer''}{5}
\contentsline {section}{\numberline {4}Code source}{5}
\contentsline {subsection}{\numberline {4.1}Fichier ``main.c''}{5}
\contentsline {subsection}{\numberline {4.2}Fichier ``memoire.h''}{8}
\contentsline {subsection}{\numberline {4.3}Fichier ``variable.h''}{10}
